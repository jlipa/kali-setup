
# Automating Kali Setup

## Description

This repo is to help me automate the setup of my Kali environment at each new release. Remember, there will be action required, especially if needing to install the Intel OpenCL dirver.

## Installation

Clone the repository

```bash
cd /tmp
git clone https://gitlab.com/jlipa/kali-setup.git
cd kali-setup
./setup.sh
```

## Additional VMWare Settings

In `*.vmx` add:

```
sharedFolder0.present = "TRUE"
sharedFolder0.enabled = "TRUE"
sharedFolder0.readAccess = "TRUE"
sharedFolder0.writeAccess = "TRUE"
sharedFolder0.hostPath = "<host-path-here>"
sharedFolder0.guestName = "<on-vm-name-not-path>"
sharedFolder0.expiration = "never"
sharedFolder.maxNum = "1"
tools.upgrade.policy = "useGlobal"
```

Update memory:

```
memsize = "8192"
```

# VM Settings

Update /etc/fstab so volumes mount at each boot:

> Per: https://communities.vmware.com/t5/VMware-Fusion-Discussions/mnt-hgfs-does-not-get-mounted-after-reboots-for-shared-folders/td-p/2889090

Add the following line to `/etc/fstab`

```vim
vmhgfs-fuse   /mnt/hgfs    fuse    defaults,allow_other    0    0
```

## Roadmap

This script and repository will be improved as I learn which tools I need in the toolbox.
