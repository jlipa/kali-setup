#!/bin/bash

install_from_repositories() {

    echo "[*] Installing Terminator"
    sudo apt-get -qq install terminator > /dev/null

    echo "[*] Installing fuzzy finder"
    sudo apt-get -qq install fzf > /dev/null

    echo "[*] Installing htop"
    sudo apt-get -qq install htop > /dev/null

    echo "[*] Installing VSCode"
    sudo apt-get -qq install code-oss > /dev/null

    echo "[*] Installing RDesktop"
    sudo apt-get -qq install rdesktop > /dev/null

    echo "[*] Installing Crowbar"
    sudo apt-get -qq install crowbar > /dev/null

    echo "[*] Installing Remmia"
    sudo apt-get -qq install remmina > /dev/null

    echo "[*] Installing PDF Crack"
    sudo apt-get -qq install pdfcrack > /dev/null

    echo "[*] Installing SecLists"
    sudo apt-get -qq install seclists > /dev/null

    echo "[*] Installing HttpTunnel"
    sudo apt-get -qq install httptunnel > /dev/null

    echo "[*] Installing Kerberoast"
    sudo apt-get -qq install kerberoast > /dev/null

    echo "[*] Installing Pure-FTPd"
    sudo apt-get -qq install pure-ftpd > /dev/null

    echo "[*] Installing Powercat"
    sudo apt-get -qq install powercat > /dev/null

    echo "[*] Installing Masscan"
    sudo apt-get -qq install masscan > /dev/null

    echo "[*] Installing exploitdb"
    sudo apt-get -qq install exploitdb > /dev/null

    echo "[*] Installing Mingw-w64"
    sudo apt-get -qq install mingw-w64 > /dev/null

    echo "[*] Installing atftp"
    sudo apt-get -qq install atftp > /dev/null

    echo "[*] Installing Shellter"
    sudo apt-get -qq install shellter > /dev/null

    echo "[*] Installing Rinetd"
    sudo apt-get -qq install rinetd > /dev/null

    echo "[*] Installing vsFTP"
    sudo apt-get -qq install vsftpd > /dev/null

    echo "[*] Installing bloodhound"
    sudo apt-get -qq install bloodhound > /dev/null

    echo "[*] Installing ltrace"
    sudo apt-get -qq install ltrace > /dev/null

    echo "[*] Installing evil-winrm"
    sudo gem install evil-winrm > /dev/null
}

install_amdcpu_hashcat() {
    ## If AMD CPU, install Intel OpenCL driver
    CPU_MODEL=$(lscpu | grep "Model name:")

    if [[ $CPU_MODEL =~ "AMD " ]]
    then
        echo "[*] Setting up Intel OpenCL for AMD CPU"

        sudo apt-get -qq install intel-opencl-icd > /dev/null
    fi

}


install_autorecon() {
    sudo apt-get -qq install python3 > /dev/null
    sudo apt-get -qq install python3-pip > /dev/null

    sudo apt-get -qq install enum4linux feroxbuster gobuster impacket-scripts nbtscan nikto nmap onesixtyone oscanner redis-tools smbclient smbmap snmp sslscan sipvicious tnscmd10g whatweb wkhtmltopdf > /dev/null

    sudo apt-get -qq install python3-venv > /dev/null
    python3 -m pip install pipx
    python3 -m pipx ensurepath

    source ~/.zshrc && 
    pipx install git+https://github.com/Tib3rius/AutoRecon.git &&
    echo 'alias autorecon="sudo $(which autorecon)"' >> ~/.zshrc
}

TOOLS_DIR=/opt/tools

setup_tools_directory() {
    sudo mkdir $TOOLS_DIR
    sudo chown kali:kali -R $TOOLS_DIR
}

install_penelope() {
    git clone --quiet https://github.com/brightio/penelope $TOOLS_DIR/penelope > /dev/null
    echo "export PATH=\$PATH:$TOOLS_DIR/penelope"  >> ~/.zshrc
}

install_enum4linux-ng() {
    git clone --quiet https://github.com/cddmp/enum4linux-ng $TOOLS_DIR/enum4linux-ng > /dev/null
    cd $TOOLS_DIR/enum4linux-ng
    python3 -m venv venv
    pip install wheel
    pip install -r requirements.txt
    echo "export PATH=\$PATH:$TOOLS_DIR/enum4linux-ng"  >> ~/.zshrc
}

install_git_tools() {
    cd $TOOLS_DIR

    echo "[--] Windows Tools [--]"
    mkdir windows-tools && cd windows-tools
    
    echo "[*] Installing GitTools"
    git clone --quiet https://github.com/internetwache/GitTools > /dev/null
    echo "export PATH=\$PATH:$(pwd)/GitTools/Extractor:$(pwd)/GitTools/Dumper:$(pwd)/GitTools/Finder"  >> ~/.zshrc

    echo "[*] Installing CredDump7"
    git clone --quiet https://github.com/CiscoCXSecurity/creddump7.git > /dev/null
    echo "export PATH=\$PATH:$(pwd)/creddump7"  >> ~/.zshrc

    echo "[*] Installing Impacket"
    git clone --quiet https://github.com/SecureAuthCorp/impacket.git > /dev/null
    (cd impacket && python3 -m pip install -r requirements.txt)
    echo "export PATH=\$PATH:$(pwd)/impacket"  >> ~/.zshrc

    echo "[*] Installing WinPeas"
    git clone --quiet https://github.com/carlospolop/privilege-escalation-awesome-scripts-suite.git > /dev/null
    echo "[*] Installing Windows Exploit Suggestor"
    git clone --quiet  https://github.com/bitsadmin/wesng.git > /dev/null

    echo "[--] Linux Tools [--]"
    cd $TOOLS_DIR
    mkdir linux-tools && cd linux-tools
    echo "[*] Installing LinEnum"
    git clone --quiet https://github.com/rebootuser/LinEnum.git > /dev/null
    echo "[*] Installing LinPeas"
    git clone --quiet https://github.com/carlospolop/privilege-escalation-awesome-scripts-suite.git > /dev/null

    echo "[*] Installing gimmeSH"
    sudo curl https://raw.githubusercontent.com/A3h1nt/gimmeSH/main/gimmeSH.sh -o /usr/bin/gimmeSH.sh
    sudo chmod +x /usr/bin/gimmeSH.sh

    mv $SETUP_DIR/tools/* $TOOLS_DIR
}


install_vscode_ext () {
    echo "[*] Install Peas-In-Color"

    cd $SETUP_DIR/extensions/vscode
    code-oss --install-extension peas_in_color
}

persist_mounts () {

    echo "vmhgfs-fuse   /mnt/hgfs    fuse    defaults,allow_other    0    0" | sudo tee -a /etc/fstab 

}
