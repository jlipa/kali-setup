#!/bin/bash

echo "[---] Setting up Kali [---]"

SETUP_DIR=$(pwd)
source $SETUP_DIR/setup-helper.sh

echo "[--] Update repo lists [--]"
sudo apt-get -qq update

install_from_repositories

echo "[--] Unzipping Rockyou.txt [--]"
(cd /usr/share/wordlists && sudo gunzip rockyou.txt.gz)

echo "[---] Setup Tools Location [---]"

setup_tools_directory

echo "[*] Installing penelope.py"

install_penelope

echo "[*] Installing enum4linux-ng"

install_enum4linux-ng

echo "[*] Installing Autorecon"

install_autorecon

echo "[*] Installing Alternative Hardware Requirements"

install_amdcpu_hashcat

echo "[---] Installing Git repository tools [---]"

install_git_tools

echo "[---] Installing VS Code extensions [---]"

echo "[---------------------------------------]"
echo "[--] For now, install manually. code-oss does not support --install-extension option [--]"
echo "[---------------------------------------]"
#install_vscode_ext

echo "[*] Edit /etc/fstab to persist volume mounts"
persist_mounts

echo "[*] Update searchsploit db"
searchsploit -u

echo "[*] Update plocate database"
sudo updatedb

echo "[*] Clean up"
rmdir ~/Music ~/Public ~/Videos ~/Templates ~/Desktop &>/dev/null


echo "[---] Reboot after this setup [---]" 
